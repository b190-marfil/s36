const Task = require("../models/task.js");

module.exports.getAllTasks=()=>{
	return Task.find({}).then(result=>{
		return result;
	});
};

module.exports.createTask = (requestBody)=>{
	let newTask = new Task({
		name: requestBody.name
	});
	return newTask.save().then((task,error)=>{
		if (error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
};

module.exports.deleteTask = (taskId)=>{
	return Task.findByIdAndRemove(taskId).then((result,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return result;
		}
	});
};

module.exports.updateTask = (taskId,newContent)=>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			result.name = newContent.name;
			return result.save().then((updatedTask,error)=>{
				if(error){
					console.log(error);
					return false;
				}else{
					return updatedTask;
				}
			})
		}
	});
};

// ACTIVITY SECTION

// Controller function for getting a specific task

	// Business Logic
	/*
		1. Get the task with the id using the Mongoose method "findById"
	*/


// Controller function for updating a task status to "complete"

	// Business Logic
	/*
		1. Get the task with the id using the Mongoose method "findById"
		2. Change the status of the document to complete (hardcoded OR using requestBody)
		3. Save the task
	*/

module.exports.getTask = (taskId)=>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return result;
		}
	});
};

module.exports.completeTask = (taskId)=>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			result.status = "Complete";
			return result.save().then((updatedTask,error)=>{
				if(error){
					console.log(error);
					return false;
				}else{
					return updatedTask;
				}
			})
		}
	});
};
