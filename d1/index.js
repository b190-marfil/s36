const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes.js");

const app = express();
const port = 3000;


mongoose.connect("mongodb+srv://zooomxc:admin@wdc028-course-booking.igawzgs.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);


let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/tasks",taskRoutes)



app.listen( port, () => console.log(`Server running at port: ${port}`) );
